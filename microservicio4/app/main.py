from flask import Flask,request, jsonify, Response
from flask_cors import CORS
from database import db
from resources import mod
from flask_jwt_extended import create_access_token
from flask_jwt_extended import JWTManager
import datetime

#Ejecutar Flask
app = Flask(__name__)
jwt = JWTManager(app)
app.config['JWT_SECRET_KEY'] = 'x1x2x3x4'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=1) # define the life span of the token
app.config['JWT_TOKEN_LOCATION'] = ['headers']
app.config['JWT_BLACKLIST_ENABLED'] = True

CORS(app)
app.register_blueprint(mod, url_prefix='/api')
mongo = db

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8082,debug=True)
