from flask import Response, request,jsonify
from database import db
import hashlib
from bson  import json_util
from bson.json_util import dumps
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity

from flask_jwt_extended import create_access_token

routes = []

@jwt_required()
def getLayawayList():
    current_user = get_jwt_identity()
    user_from_db = db.db.users.find_one({'username' : current_user})
    print(user_from_db['_id'])
    if user_from_db:
        try:
            LawayList = db.db.user_comics.find_one({'_user': user_from_db['_id']})
            return jsonify({'LawayList' : LawayList["comics"] }), 200
        except:
            return jsonify({'msg': 'Not Comics'}), 404
    else:
        return jsonify({'msg': 'User not found'}), 404


routes.append(dict(
    rule='/getLayawayList/',
    view_func=getLayawayList,
    methods=["POST"]))