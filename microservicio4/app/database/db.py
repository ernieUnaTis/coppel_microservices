from flask import Flask
from flask_pymongo import pymongo
import os
from dotenv import load_dotenv
load_dotenv('app/.env')


CONNECTION_STRING = "mongodb+srv://"+os.environ.get('USER_MONGO')+":"+os.environ.get('PASS_MONGO')+"@cluster0.4prchtt.mongodb.net/?retryWrites=true&w=majority"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('flask_mongo_dbatlas_comics')

