# Coppel Microservices


# Título del Proyecto

Ejemplo de MicroServicios
## Software 🚀

* FastApi (MicroServicio1)
* Flask
* MongoDb
* Python 3.9
* Docker


### Pre-requisitos 📋



```
Tener Docker y Docker-Compose instlado
```

### Instalación 🔧

docker-compose up

## Ejecutando las pruebas ⚙️

* Microservicio1 http://127.0.0.1:8081/doc
* Microservicio1 http://127.0.0.1:8082
* Microservicio1 http://127.0.0.1:8083
* Microservicio1 http://127.0.0.1:8084

** Se Adjunta colección en Insomina para las pruebas

## Despliegue 📦



## Expresiones de Gratitud 🎁

* Gracias por la oportunidad 📢

