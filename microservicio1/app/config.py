import os
from pathlib import Path
from dotenv import load_dotenv
from pydantic import BaseSettings

env_path = Path(".") / ".env"
load_dotenv(dotenv_path=env_path)

class Settings(BaseSettings):
    TS: str
    PRIVATE_KEY: str
    PUBLIC_KEY: str
    
    class Config:
        env_file = "app/.env"
        env_file_encoding = 'utf-8'
