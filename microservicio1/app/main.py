from fastapi import FastAPI, Query
from app.api import api


app = FastAPI()



@app.get("/api/v1/searchComics/")
def search(offset: int = 0, limit: int = 20, text: str=None,filtro: str=None):
    return api.search(offset,
                      limit,
                      text,
                      filtro)

