
from app.config import Settings

import hashlib
import requests
import json

settings = Settings()

def generate_hash():
    str2hash=settings.TS+settings.PRIVATE_KEY+settings.PUBLIC_KEY
    result = hashlib.md5(str2hash.encode())
    return result.hexdigest()


def search_characters(offset,limit,text):
    hash = generate_hash()
    query = {'ts':settings.TS, 'apikey':settings.PUBLIC_KEY,'hash':hash,"limit":limit,"offset":offset,"name":text}
    response_api_marvel = requests.get("http://gateway.marvel.com/v1/public/characters",
                             params=query)
  
    response = []
    jsonObject = json.loads(json.dumps(response_api_marvel.json()))
    for value in jsonObject["data"]["results"]:
        response_character={}
        response_character["id"] = value["id"]
        response_character["name"] = value["name"]
        response_character["image"] = value["thumbnail"]["path"]+"."+value["thumbnail"]["extension"]
        response_character["appearances"] = int(value["comics"]["available"])+int(value["series"]["available"])+int(value["stories"]["available"])+int(value["events"]["available"])
        response.append(response_character)
    return response

def search_comics(offset,limit,text):
    hash = generate_hash()
    query = {'ts':settings.TS, 'apikey':settings.PUBLIC_KEY,'hash':hash,"limit":limit,"offset":offset,"title":text}
    response_api_marvel = requests.get("http://gateway.marvel.com/v1/public/comics",
                             params=query)
    response = []


    jsonObject = json.loads(json.dumps(response_api_marvel.json()))
    for value in jsonObject["data"]["results"]:
        response_comic={}
        response_comic["id"] = value["id"]
        response_comic["title"] = value["title"]
        response_comic["image"] = value["thumbnail"]["path"]+"."+value["thumbnail"]["extension"]
        response_comic["onSaleDate"] = value["dates"][0]["date"]
        response.append(response_comic)
    return response
