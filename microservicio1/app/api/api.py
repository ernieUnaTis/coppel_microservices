from app.utils import api_marvel
import json



def search(offset,limit,text,filtro):
    response = []

    if text is None:
        characters = api_marvel.search_characters(offset,limit,text)
        print(characters)
        for character in characters:
            response.append(character["name"])
        return response
    elif filtro is None:
        character = api_marvel.search_characters(offset,limit,text)
        #response.append(character)
        comic = api_marvel.search_comics(offset,limit,text)
        #response.append(comic)
        return character + comic
    else:
        if filtro=="Comic":
            comic = api_marvel.search_comics(offset,limit,text)
            #response.append(comic)
            return comic
        else:
            character = api_marvel.search_characters(offset,limit,text)
            #response.append(character)
            return character
