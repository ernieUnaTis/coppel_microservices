from flask import Response, request,jsonify
from database import db
from utils import api_ms1
import hashlib
from bson  import json_util
from bson.json_util import dumps
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity

from flask_jwt_extended import create_access_token

routes = []

@jwt_required()
def addToLayaway():
    data = request.get_json()
    comic = data.get("comic")
    current_user = get_jwt_identity()
    user_from_db = db.db.users.find_one({'username' : current_user})
    if user_from_db:
        resultado = api_ms1.search_comics(comic)
        db.db.user_comics.update_one({'_user': user_from_db['_id']}, {'$push': {'comics': resultado}}, upsert = True)
        return jsonify({'msg': 'Comic added to User collection successfully'}), 201
    else:
        return jsonify({'msg': 'User not found'}), 404


routes.append(dict(
    rule='/addToLayaway/',
    view_func=addToLayaway,
    methods=["POST"]))