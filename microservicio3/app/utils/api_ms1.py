import requests
import json

def search_comics(comic):
    query = {'text':comic, 'filtro':"Comic"}
    response_api_marvel = requests.get("http://backend_ms1:8000/api/v1/searchComics/",
                                        params=query)
    response = json.loads(json.dumps(response_api_marvel.json()))
    print(response)
    return response