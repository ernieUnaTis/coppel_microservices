from flask import Response, request,jsonify
from database import db
import hashlib
from bson  import json_util
from bson.json_util import dumps
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity

from flask_jwt_extended import create_access_token




routes = []

def register():
    try:
        data = request.get_json()
        nombre = data.get("nombre")
        edad = data.get("edad")
        username = data.get("username")
        password = hashlib.sha256(data.get("password").encode("utf-8")).hexdigest()
        doc = db.db.users.find_one({"username": username})
        if not doc:
            db.db.users.insert_one({"name": nombre,
                                            "edad":edad,
                                            "username":username,
                                            "password":password})
            return json_util.dumps({'msg': 'User created successfully'}), 201
        else:
            return json_util.dumps({'msg': 'Username already exists'}), 409
    except:
        return json_util.dumps({'msg': 'Username not create'}), 404

routes.append(dict(
    rule='/users/register',
    view_func=register,
    methods=["POST"]))


def login():
    login_details = request.get_json()
    print(login_details)
    user_from_db = db.db.users.find_one({'username': login_details['username']})
    if user_from_db:
        encripted_password = hashlib.sha256(login_details['password'].encode("utf-8")).hexdigest()
        if encripted_password == user_from_db['password']:
            access_token = create_access_token(identity=user_from_db['username']) # create jwt token
            return jsonify(access_token=access_token), 200

    return jsonify({'msg': 'The username or password is incorrect'}), 401


routes.append(dict(
    rule='/users/login/',
    view_func=login,
    methods=["POST"]))

@jwt_required()
def profile():
    print("hola")
    current_user = get_jwt_identity()
    user_from_db = db.db.users.find_one({'username' : current_user})
    if user_from_db:
        del user_from_db['_id'], user_from_db['password'] 
        return jsonify({'profile' : user_from_db }), 200
    else:
        return jsonify({'msg': 'Profile not found'}), 404


routes.append(dict(
    rule='/users/profile/',
    view_func=profile,
    methods=["GET"]))