from flask import Blueprint

mod = Blueprint('resources', __name__)
from .users.users import routes as users_routes

for r in users_routes:
    mod.add_url_rule(
        r['rule'],
        endpoint=r.get('endpoint', None),
        view_func=r['view_func'],
        methods=r['methods'])
